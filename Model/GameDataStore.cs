﻿using System;
using System.IO.IsolatedStorage;
using Min_Mang.WP.Helpers;
using System.Linq;

namespace Min_Mang.WP.Model
{
    public static class GameDataStore
    {
        private const string NEW_GAME_SETTINGS_KEY = "Min-Mang.NewGameSettings";
        private const string OPTIONS_KEY = "Min-Mang.Options";
        private static NewGameSettings _newGameSettings;
        private static Options _options;
        private static readonly IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;

        public static event EventHandler PropertyUpdated;

        /// <summary>
        /// Gets or sets the new game settings, loading the data from isolated storage
        /// (if there is any saved data) on the first access.
        /// </summary>
        public static NewGameSettings NewGameSettings
        {
            get
            {
                if (_newGameSettings == null)
                {
                    if (appSettings.Contains(NEW_GAME_SETTINGS_KEY))
                    {
                        _newGameSettings = (NewGameSettings)appSettings[NEW_GAME_SETTINGS_KEY];
                    }
                    else
                    {
                        _newGameSettings = new NewGameSettings
                        {
                            PlayerType1 = StaticProperties.PlayerTypes.First().Key,
                            PlayerType2 = StaticProperties.PlayerTypes.Last().Key,
                            Difficulty1 = StaticProperties.Difficulties.First().Key,
                            Difficulty2 = StaticProperties.Difficulties.First().Key,
                        };
                    }
                }
                return _newGameSettings;
            }
            set
            {
                _newGameSettings = value;
                NotifyPropertyChanged();
            }
        }

        public static Options Options
        {
            get
            {
                if (_options == null)
                {
                    if (appSettings.Contains(OPTIONS_KEY))
                    {
                        _options = (Options)appSettings[OPTIONS_KEY];
                    }
                    else
                    {
                        _options = new Options
                        {
                            PlayerOneColor = ColorExtensions.AccentColors()[7],
                            PlayerTwoColor = ColorExtensions.AccentColors()[8],
                        };
                    }
                }
                return _options;
            }
            set
            {
                _options = value;
                NotifyPropertyChanged();
            }
        }

        private static void NotifyPropertyChanged()
        {
            var handler = PropertyUpdated;
            if (handler != null) handler(null, null);
        }

        public static void SaveNewGameSettings(Action errorCallback)
        {
            //try
            //{
                appSettings[NEW_GAME_SETTINGS_KEY] = NewGameSettings;
                appSettings.Save();
                NotifyPropertyChanged();
            //}
            //catch (IsolatedStorageException)
            //{
            //    errorCallback();
            //}
        }

        public static void SaveOptions(Action errorCallback)
        {
            try
            {
                appSettings[OPTIONS_KEY] = Options;
                appSettings.Save();
                NotifyPropertyChanged();
            }
            catch (IsolatedStorageException)
            {
                errorCallback();
            }
        }
    }
}
