﻿using System.ComponentModel;
using System.Windows.Media;

namespace Min_Mang.WP.Model
{
    public class Options : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _playerOneColor;
        public string PlayerOneColor
        {
            get { return _playerOneColor; }
            set
            {
                if (_playerOneColor == value) return;
                _playerOneColor = value;
                NotifyPropertyChanged("PlayerOneColor");
            }
        }

        private string _playerTwoColor;
        public string PlayerTwoColor
        {
            get { return _playerTwoColor; }
            set
            {
                if (_playerTwoColor == value) return;
                _playerTwoColor = value;
                NotifyPropertyChanged("PlayerTwoColor");
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (PropertyChanged != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
