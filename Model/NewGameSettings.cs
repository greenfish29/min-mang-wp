﻿using System.ComponentModel;
using System.Collections.ObjectModel;
using Min_Mang.Core.WP;

namespace Min_Mang.WP.Model
{
    public class NewGameSettings : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _playerType1;
        public string PlayerType1
        {
            get { return _playerType1; }
            set
            {
                if (_playerType1 == value) return;
                _playerType1 = value;
                NotifyPropertyChanged("PlayerType1");
            }
        }

        private string _playerType2;
        public string PlayerType2
        {
            get { return _playerType2; }
            set
            {
                if (_playerType2 == value) return;
                _playerType2 = value;
                NotifyPropertyChanged("PlayerType2");
            }
        }

        private string _difficulty1;
        public string Difficulty1
        {
            get { return _difficulty1; }
            set
            {
                if (_difficulty1 == value) return;
                _difficulty1 = value;
                NotifyPropertyChanged("Difficulty1");
            }
        }

        private string _difficulty2;
        public string Difficulty2
        {
            get { return _difficulty2; }
            set
            {
                if (_difficulty2 == value) return;
                _difficulty2 = value;
                NotifyPropertyChanged("Difficulty2");
            }
        }

        public GameState State { get; set; }

        public FieldType WhoMoves { get; set; }

        private GameDesk _gameDeskState;
        public GameDesk GameDeskState {
            get
            {
                return _gameDeskState; //== null ? new Core.WP.GameDesk(7, 7) : _gameDeskState;
            }
            set
            {
                _gameDeskState = value;
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (PropertyChanged != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
