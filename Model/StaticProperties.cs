﻿using System.Collections.Generic;
using Min_Mang.WP.Resources;

namespace Min_Mang.WP.Model
{
    public class StaticProperties
    {
        private static Dictionary<string, string> _types = new Dictionary<string, string> {
            { "human", AppResources.Human },
            { "computer", AppResources.Computer }
        };
        private static Dictionary<string, string> _difficulties = new Dictionary<string, string> {
            { "easy", AppResources.Easy },
            { "normal", AppResources.Normal },
            { "hard", AppResources.Hard }
        };

        public static Dictionary<string,string> PlayerTypes
        {
            get { return _types; }
        }

        public static Dictionary<string, string> Difficulties
        {
            get { return _difficulties; }
        }

    }
}
