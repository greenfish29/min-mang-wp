﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Min_Mang.Core.WP;
using Min_Mang.WP.Helpers;
using Min_Mang.WP.Model;
using Min_Mang.WP.Resources;

namespace Min_Mang.WP
{
    public partial class MainPage : PhoneApplicationPage
    {
        private const int WIDTH = 7;
        private const int HEIGHT = 7;
        private const int MARGIN = 24;
        private const int STROKE_THICKNESS = 2;
        private Point[] _piecesPoints;
        private double _fieldSize;
        private double _pieceSize;
        private int _id = -1;
        private Director _director;
        private App _app = (App)App.Current;
        private Move _lastMove;
        private int[] _capturedPiecesIndexes;
        private List<Path> _capturedPieces = new List<Path>();
        private PlayerType _player1, _player2;
        private bool _canMove, _wasAboutPageNavigated;
        private ApplicationBarIconButton _playBtn, _pauseBtn;
        private ApplicationBarMenuItem _restartItem, _aboutItem;
        private SolidColorBrush _brushOne, _brushTwo;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            InitAppBar(_app.NewGameSettings.State);
            _player1 = ConvertToPlayerType(_app.NewGameSettings.PlayerType1);
            _player2 = ConvertToPlayerType(_app.NewGameSettings.PlayerType2);
            _brushOne = AccentColorNameToBrush.ColorNameToBrush[_app.Options.PlayerOneColor];
            _brushTwo = AccentColorNameToBrush.ColorNameToBrush[_app.Options.PlayerTwoColor];
            _director = Director.Instance;
            StartNewGame();
            poneCount.Foreground = _brushOne;
            ptwoCount.Foreground = _brushTwo;
            SetCountText(FieldType.P_ONE);
            SetCountText(FieldType.P_TWO);
            SetPlayersTypeText();
            pausedInfo.Visibility = _app.NewGameSettings.State == GameState.PAUSED ? Visibility.Visible : Visibility.Collapsed;
        }

        private void StartNewGame()
        {
            _director.StartNewGame(
                _app.NewGameSettings.GameDeskState == null ? new GameDesk(7, 7) : _app.NewGameSettings.GameDeskState,
                _player1,
                _player2,
                ConvertToDifficultyType(_app.NewGameSettings.Difficulty1),
                ConvertToDifficultyType(_app.NewGameSettings.Difficulty2),
                _app.NewGameSettings.State,
                _app.NewGameSettings.WhoMoves);
            _canMove = CanMove(_app.NewGameSettings.WhoMoves);
            InitDesk(_director.GameDesk);
        }

        private void InitAppBar(GameState state)
        {
            ApplicationBar appBar = new ApplicationBar();
            appBar.BackgroundColor = (Color)App.Current.Resources["PhoneBackgroundColor"];

            // Play button.
            _playBtn = new ApplicationBarIconButton(new Uri("/Images/play.png", UriKind.Relative));
            _playBtn.Click += Play_Click;
            _playBtn.Text = AppResources.Play;
            appBar.Buttons.Add(_playBtn);
            // Pause button.
            _pauseBtn = new ApplicationBarIconButton(new Uri("/Images/pause.png", UriKind.Relative));
            _pauseBtn.Click += Pause_Click;
            _pauseBtn.Text = AppResources.Pause;
            appBar.Buttons.Add(_pauseBtn);

            if (state == GameState.RUNNING)
            {
                _playBtn.IsEnabled = false;
            }
            else
            {
                _pauseBtn.IsEnabled = false;
            }

            _restartItem = new ApplicationBarMenuItem(AppResources.Restart);
            _restartItem.Click += new EventHandler(RestartItem_Click);
            appBar.MenuItems.Add(_restartItem);
            _aboutItem = new ApplicationBarMenuItem(AppResources.AboutPageTitle);
            _aboutItem.Click += new EventHandler(AboutItem_Click);
            appBar.MenuItems.Add(_aboutItem);

            ApplicationBar = appBar;
        }

        void AboutItem_Click(object sender, EventArgs e)
        {
            _wasAboutPageNavigated = true;
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        void RestartItem_Click(object sender, EventArgs e)
        {
            _app.NewGameSettings.GameDeskState = null;
            _app.NewGameSettings.WhoMoves = FieldType.P_ONE;
            _app.NewGameSettings.State = _director.GameState;

            _director.Pause();
            Thread.Sleep(1000);

            StartNewGame();
        }

        /// <summary>
        /// Initialize game desk.
        /// </summary>
        private void InitDesk(GameDesk desk)
        {
            GameDeskPanel.Children.Clear();

            int deskWidth = WIDTH - 1;
            int deskHeight = HEIGHT - 1;
            // Set game pieces.
            _piecesPoints = new Point[WIDTH * HEIGHT];
            // Set game desk panel width (wrapping).
            GameDeskPanel.Width = Application.Current.Host.Content.ActualWidth;
            // Set game desk panel margin.
            GameDeskPanel.Margin = new Thickness(MARGIN);
            // Get field size.
            _fieldSize = (Application.Current.Host.Content.ActualWidth - (MARGIN * 2)) / deskWidth;
            // Get piece size.
            _pieceSize = _fieldSize / 1.6;

            // Draw game fields.
            for (int i = 0; i < deskWidth * deskHeight; i++)
            {
                Rectangle rect = new Rectangle();
                rect.Width = rect.Height = _fieldSize;
                rect.Stroke = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
                rect.StrokeThickness = STROKE_THICKNESS;
                GameDeskPanel.Children.Add(rect);
                rect.Margin = new Thickness(0);
            }

            SetPiecesLocation(desk);
            RedrawDesk(desk);
        }

        /// <summary>
        /// Take care about game desk redrawing.
        /// </summary>
        /// <param name="desk">The game desk.</param>
        /// <param name="last">Id of most upper piece (animation required).</param>
        /// <returns>The ellipse geometry of piece, which was manipulated.</returns>
        private EllipseGeometry RedrawDesk(GameDesk desk, int last = -1)
        {
            PiecesContainer.Children.Clear();
            SolidColorBrush brush = null;
            EllipseGeometry piece = null;

            for (int i = 0; i < _piecesPoints.Length; i++)
            {
                if (i == last) continue;
                bool isCaptured = false;

                // Check, if piece was captured in last move (animation).
                if (_capturedPiecesIndexes != null)
                {
                    isCaptured = _capturedPiecesIndexes.Contains(i);
                }

                // No drawing for empty fields.
                if (desk.GetFieldValue(i) == FieldType.EMPTY && !isCaptured) continue;

                // Select brush color.
                if (isCaptured)
                {
                    brush = desk.GetFieldValue(_lastMove.To) == FieldType.P_ONE ? _brushTwo : _brushOne;
                }
                else 
                {
                    brush = (desk.GetFieldValue(i) == FieldType.P_ONE) ? _brushOne : _brushTwo;
                }

                if(isCaptured)
                {
                    _capturedPieces.Add(DrawPiece(i, brush).First().Value);
                }
                else if (_lastMove != null && i == _lastMove.To)
                {
                    piece = DrawPiece(i, brush).Keys.First();
                }
                else
                {
                    DrawPiece(i, brush);
                }
            }

            if(last >= 0) piece = DrawPiece(last, (desk.GetFieldValue(last) == FieldType.P_ONE) ? _brushOne : _brushTwo).Keys.First();

            return piece;
        }

        private Dictionary<EllipseGeometry, Path> DrawPiece(int id, Brush brush)
        {
            EllipseGeometry myEllipseGeometry = new EllipseGeometry();
            myEllipseGeometry.Center = new Point(_piecesPoints[id].X, _piecesPoints[id].Y);
            myEllipseGeometry.RadiusX = _pieceSize / 2;
            myEllipseGeometry.RadiusY = _pieceSize / 2;

            Path myPath = new Path();
            myPath.Fill = brush;
            myPath.Stroke = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
            myPath.StrokeThickness = STROKE_THICKNESS;
            myPath.Data = myEllipseGeometry;

            PiecesContainer.Children.Add(myPath);

            return new Dictionary<EllipseGeometry, Path>() { { myEllipseGeometry, myPath } };
        }

        private void SetPiecesLocation(GameDesk desk)
        {
            for (int i = 0; i < _piecesPoints.Length; i++)
            {
                double x = MARGIN + (i % WIDTH) * _fieldSize;
                double y = MARGIN + (i / WIDTH) * _fieldSize;
                _piecesPoints[i] = new Point(x, y);
            }
        }

        /// <summary>
        /// Select the piece.
        /// </summary>
        /// <param name="point">Point, where was clicked.</param>
        /// <returns>Field id, if piece was clicked, otherwise -1.</returns>
        private int IsPieceTaped(Point point)
        {
            double size = _pieceSize / 2;
            for (int i = 0; i < _piecesPoints.Length; i++)
            {
                //if (_director.GameDesk.GetFieldValue(i) == FieldType.EMPTY) continue;

                Point p = _piecesPoints[i];
                Rect r = new Rect(p.X - size, p.Y - size, _pieceSize, _pieceSize);
                RectangleGeometry rec = new RectangleGeometry();
                rec.Rect = r;

                if (r.Contains(point))
                {
                    return i;
                }
            }
            return -1;
        }

        private void PiecesContainer_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_canMove) return;

            Point p = e.GetPosition(PiecesContainer);
            int prevId = _id;
            _id = IsPieceTaped(p);
            FieldType value = _director.GameDesk.GetFieldValue(_id);

            // If piece was not yet selected, or if was selected new piece.
            if (prevId < 0 || value == _director.WhoMoves)
            {
                if (_id >= 0 && value == _director.WhoMoves)
                {
                    RedrawDesk(_director.GameDesk, _id);
                    DrawAllPossibleMoves(_director.GameDesk);
                }
            }
            else if (_id >= 0 && value == FieldType.EMPTY)
            {
                Move m = new Move(prevId, _id);
                _canMove = CanMove(_director.GameDesk.GetFieldValue(prevId) == FieldType.P_ONE ? FieldType.P_TWO : FieldType.P_ONE);
                _director.Move(m);
            }
            else
            {
                _id = prevId;
            }
        }

        private void _director_PieceMoved(Move move, int[] capturedPieces)
        {
            Dispatcher.BeginInvoke(delegate()
            {
                _lastMove = move;
                if (_id == -1) _id = _lastMove.To;
                _capturedPiecesIndexes = capturedPieces;

                EllipseGeometry piece = RedrawDesk(_director.GameDesk, _id);

                PointAnimation animation = new PointAnimation();
                animation.From = _piecesPoints[_lastMove.From];
                animation.To = _piecesPoints[_lastMove.To];
                animation.Duration = TimeSpan.FromSeconds(0.7);

                Storyboard.SetTarget(animation, piece);
                Storyboard.SetTargetProperty(animation, new PropertyPath(EllipseGeometry.CenterProperty));

                Storyboard ellipseStoryboard = new Storyboard();
                ellipseStoryboard.Children.Add(animation);
                ellipseStoryboard.Completed += new EventHandler(ellipseStoryboard_Completed);

                ellipseStoryboard.Begin();
                _canMove = false;
                _id = -1;
            });
        }

        private void ellipseStoryboard_Completed(object sender, EventArgs e)
        {
            SetCountText(_director.WhoMoves);

            if (_capturedPieces.Count != 0)
            {
                foreach (Path path in _capturedPieces)
                {
                    PiecesContainer.Children.Remove(path);
                }

                _capturedPiecesIndexes = null;
                _capturedPieces.Clear();
            }

            if (_director.GameState == GameState.GAME_OVER)
            {
                GameEnd();
                return;
            }

            if(_director.GameState == GameState.RUNNING)
                _canMove = CanMove(_director.WhoMoves);
        }

        private void DrawAllPossibleMoves(GameDesk desk)
        {
            bool alone = desk.CountPieces(desk.GetFieldValue(_id)) == 1 ? true : false;
            Move[] moves = alone ? Referee.PossibleSkipMoves(desk,_id) : Referee.GetPossibleMoves(desk, _id);
            SolidColorBrush brush = new SolidColorBrush(Colors.LightGray);
            brush.Opacity = 0.5;

            foreach (Move m in moves)
            {
                DrawPiece(m.To, brush);
            }
        }

        private PlayerType ConvertToPlayerType(string typeString)
        {
            return (PlayerType)StaticProperties.PlayerTypes.Keys.ToList().IndexOf(typeString);
        }

        private Difficulty ConvertToDifficultyType(string diffString)
        {
            return (Difficulty)StaticProperties.Difficulties.Keys.ToList().IndexOf(diffString) + 1;
        }

        private void Pause_Click(object sender, EventArgs e)
        {
            _director.Pause();
            _canMove = false;
            _pauseBtn.IsEnabled = false;
            _restartItem.IsEnabled = false;
            _aboutItem.IsEnabled = false;
            Thread thread = new Thread(SleepThread);
            thread.Start();
            pausedInfo.Visibility = Visibility.Visible;
        }

        private void SleepThread()
        {
            Thread.Sleep(1000);
            _playBtn.IsEnabled = true;
            _restartItem.IsEnabled = true;
            _aboutItem.IsEnabled = true;
        }

        private void Play_Click(object sender, EventArgs e)
        {
            _director.Unpause();
            _canMove = CanMove(_director.WhoMoves);
            _playBtn.IsEnabled = false;
            _pauseBtn.IsEnabled = true;
            pausedInfo.Visibility = Visibility.Collapsed;
        }

        private void GameEnd()
        {
            _canMove = false;
            _playBtn.IsEnabled = false;
            _pauseBtn.IsEnabled = false;
            _app.NewGameSettings.GameDeskState = null;
            _director.PieceMoved -= new Director.PieceEventHandler(_director_PieceMoved);
            //Thread t = new Thread(delegate()
            //{
                Dispatcher.BeginInvoke(() =>
                {
                    if (_director.GameDesk.CountPieces(FieldType.P_ONE) == 0 || _director.GameDesk.CountPieces(FieldType.P_TWO) == 0)
                    {
                        string winner = _director.WhoMoves == FieldType.P_ONE ? "2" : "1";
                        MessageBox.Show(String.Format(AppResources.Winner, winner), AppResources.WinnerTitle, MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show(AppResources.Tie, AppResources.TieTitle, MessageBoxButton.OK);
                    }

                    NavigationService.Navigate(new Uri("/MainMenuPage.xaml", UriKind.Relative));
                });
            //});
            //t.Start();
        }

        private bool CanMove(FieldType player)
        {
            if (_director.GameState == GameState.PAUSED) return false;

            if (player == FieldType.P_ONE)
            {
                return _player1 == PlayerType.HUMAN ? true : false;
            }
            else
            {
                return _player2 == PlayerType.HUMAN ? true : false;
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            _director.PieceMoved -= new Director.PieceEventHandler(_director_PieceMoved);

            if (_director.GameState != GameState.GAME_OVER)
            {
                _app.NewGameSettings.State = _director.GameState;
                _app.NewGameSettings.WhoMoves = _director.WhoMoves;
                _app.NewGameSettings.GameDeskState = _director.GameDesk;
            }
            _director.Pause();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _director.PieceMoved += new Director.PieceEventHandler(_director_PieceMoved);

            if (_wasAboutPageNavigated)
            {
                if (_app.NewGameSettings.State == GameState.RUNNING)
                {
                    _director.Unpause();
                }
                _wasAboutPageNavigated = false;
            }

        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);

            if (NavigationService.BackStack.First().Source.OriginalString == "/NewGamePage.xaml")
            {
                NavigationService.RemoveBackEntry();
            }   
        }

        private void SetCountText(FieldType player)
        {
            if (player == FieldType.P_ONE)
            {
                poneCount.Text = _director.GameDesk.CountPieces(FieldType.P_ONE).ToString();
            }
            else
            {
                ptwoCount.Text = _director.GameDesk.CountPieces(FieldType.P_TWO).ToString();
            }
        }

        private void SetPlayersTypeText()
        {
            if (_player1 == PlayerType.HUMAN)
                poneType.Text += AppResources.Human;
            else
            {
                poneType.Text += AppResources.Computer;

                switch (_app.NewGameSettings.Difficulty1)
                {
                    case "easy":
                        poneType.Text += " (" + AppResources.Easy + ")";
                        break;
                    case "normal":
                        poneType.Text += " (" + AppResources.Normal + ")";
                        break;
                    case "hard":
                        poneType.Text += " (" + AppResources.Hard + ")";
                        break;
                }
            }

            if (_player2 == PlayerType.HUMAN)
                ptwoType.Text += AppResources.Human;
            else
            {
                ptwoType.Text += AppResources.Computer;

                switch (_app.NewGameSettings.Difficulty2)
                {
                    case "easy":
                        ptwoType.Text += " (" + AppResources.Easy + ")";
                        break;
                    case "normal":
                        ptwoType.Text += " (" + AppResources.Normal + ")";
                        break;
                    case "hard":
                        ptwoType.Text += " (" + AppResources.Hard + ")";
                        break;
                }
            }
        }
    }
}