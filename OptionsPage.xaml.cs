﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Min_Mang.WP.Helpers;

namespace Min_Mang.WP
{
    public partial class OptionsPage : PhoneApplicationPage
    {
        private bool _pageConstructed;
        private App _app = (App)App.Current;

        public OptionsPage()
        {
            InitializeComponent();
            //DataContext = ColorExtensions.AccentColors();
            _pageConstructed = true;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            List<string> p1PossibleColors = ColorExtensions.AccentColors().ToList();
            lpPlayerOneColor.ItemsSource = p1PossibleColors;
            List<string> p2PossibleColors = ColorExtensions.AccentColors().ToList();
            lpPlayerTwoColor.ItemsSource = p2PossibleColors;

            if (_pageConstructed)
            {
                _pageConstructed = false;

                lpPlayerOneColor.SelectedItem = _app.Options.PlayerOneColor;
                lpPlayerTwoColor.SelectedItem = _app.Options.PlayerTwoColor;
            }

            
            p1PossibleColors.Remove(lpPlayerTwoColor.SelectedItem.ToString());
            p2PossibleColors.Remove(lpPlayerOneColor.SelectedItem.ToString());
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            _app.Options.PlayerOneColor = lpPlayerOneColor.SelectedItem.ToString();
            _app.Options.PlayerTwoColor = lpPlayerTwoColor.SelectedItem.ToString();
        }
    }
}