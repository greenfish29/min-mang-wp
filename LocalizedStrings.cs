﻿using Min_Mang.WP.Resources;

namespace Min_Mang.WP
{
    public class LocalizedStrings
    {
        private static AppResources _localizedResources = new AppResources();

        public AppResources LocalizedResources { get { return _localizedResources; } }
    }
}
