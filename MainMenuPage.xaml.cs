﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using Min_Mang.Core.WP;

namespace Min_Mang.WP
{
    public partial class MainMenuPage : PhoneApplicationPage
    {
        private App _app = (App)App.Current;

        public MainMenuPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            ResumeButton.Visibility = _app.NewGameSettings.GameDeskState == null ? Visibility.Collapsed : Visibility.Visible;
        }

        private void NewGameButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewGamePage.xaml", UriKind.Relative));
        }

        private void OptionsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/OptionsPage.xaml", UriKind.Relative));
        }

        private void ResumeButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/GameBoardPage.xaml", UriKind.Relative));
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }
    }
}