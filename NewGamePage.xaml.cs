﻿using System;
using System.Linq;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Min_Mang.WP.Model;
using Min_Mang.WP.Resources;
using System.Windows.Media;

namespace Min_Mang.WP
{
    public partial class NewGamePage : PhoneApplicationPage
    {
        private const string NEW_GAME_INFO_KEY = "NewGameInfo";
        private const string HAS_UNSAVED_CHANGES_KEY = "HasUnsavedChanges";
        private App _app = (App)App.Current;
        private bool _pageConstructed;

        public NewGamePage()
        {
            InitializeComponent();
            DataContext = _app.NewGameSettings;
            // Start button.
            ApplicationBar appBar = new ApplicationBar();
            appBar.BackgroundColor = (Color)App.Current.Resources["PhoneBackgroundColor"];
            ApplicationBarIconButton startBtn = new ApplicationBarIconButton(new Uri("/Images/check.png", UriKind.Relative));
            startBtn.Click +=new EventHandler(StartNewGame_Click);
            startBtn.Text = AppResources.StartNewGame;
            appBar.Buttons.Add(startBtn);
            ApplicationBar = appBar;
            _pageConstructed = true;
            Type1.ItemsSource = StaticProperties.PlayerTypes.Values;
            Type2.ItemsSource = StaticProperties.PlayerTypes.Values;
            Difficulty1.ItemsSource = StaticProperties.Difficulties.Values;
            Difficulty2.ItemsSource = StaticProperties.Difficulties.Values;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (_pageConstructed)
            {
                SetValues();
                _pageConstructed = false;
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (!e.IsNavigationInitiator)
            {
                GetValues();
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);

            GetValues();
        }

        private void GetValues()
        {
            var settings = _app.NewGameSettings;

            settings.PlayerType1 = StaticProperties.PlayerTypes.Keys.ToList()[Type1.SelectedIndex];
            settings.PlayerType2 = StaticProperties.PlayerTypes.Keys.ToList()[Type2.SelectedIndex];
            settings.Difficulty1 = StaticProperties.Difficulties.Keys.ToList()[Difficulty1.SelectedIndex];
            settings.Difficulty2 = StaticProperties.Difficulties.Keys.ToList()[Difficulty2.SelectedIndex];
        }

        private void SetValues()
        {
            var settings = _app.NewGameSettings;

            Type1.SelectedIndex = StaticProperties.PlayerTypes.Keys.ToList().IndexOf(settings.PlayerType1);
            Type2.SelectedIndex = StaticProperties.PlayerTypes.Keys.ToList().IndexOf(settings.PlayerType2);
            Difficulty1.SelectedIndex = StaticProperties.Difficulties.Keys.ToList().IndexOf(settings.Difficulty1);
            Difficulty2.SelectedIndex = StaticProperties.Difficulties.Keys.ToList().IndexOf(settings.Difficulty2);
        }

        private void StartNewGame_Click(object sender, EventArgs e)
        {
            GetValues();
            _app.NewGameSettings.WhoMoves = Core.WP.FieldType.P_ONE;
            _app.NewGameSettings.State = Core.WP.GameState.RUNNING;
            _app.NewGameSettings.GameDeskState = null;
            NavigationService.Navigate(new Uri("/GameBoardPage.xaml", UriKind.Relative));
        }
    }
}