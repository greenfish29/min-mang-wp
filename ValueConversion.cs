﻿using System.Windows.Data;
using System;
using System.Globalization;
using Min_Mang.WP.Model;

namespace Min_Mang.WP
{
    public class TypeToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int state = (int)value;

            return state == 0 ? false : true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}